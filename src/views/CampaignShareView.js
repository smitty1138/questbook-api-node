import Campaign from '../models/Campaign';
import User from '../models/User';
import { sendInvite } from '../utils/mailer';

export const CampaignShareView = {
  shareCampaign: async (request, h) => {
    const { params: { campaignHash }, auth, payload } = request;
    const { recipients } = payload;
    const { credentials: { id = '' } } = auth;
    if(!id) {
      return h.response('Only a valid user may update campaigns.').code(403);
    }
    try {
      const campaign = await Campaign.getCampaignByHash(campaignHash);
      if (id !== campaign.owner_id) {
        return h.response('User is not permitted to modify this campaign.').code(401);
      }
      const owner = await User.getUser(campaign.owner_id);
      await sendInvite(recipients, campaign, owner);
      return 'Invites successfully sent';
    } catch (exception) {
      console.error(exception);
      if(exception.name === 'ModelNotFoundError') {
        return h.response('Could not find campaign.').code(404);
      }
      return h.response('An unknown error occurred.').code(500);
    }
  },
}

export default CampaignShareView;