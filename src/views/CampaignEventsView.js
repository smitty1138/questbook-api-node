import { PassThrough } from 'stream';
import { INIT_EVENT, subscribeToCampaign, removeSubscriber } from '../utils/events';

const CampaignEventsView = {
  subscribeToCampaign: (request, h) => {
    const { params, query } = request;
    const { campaignHash } = params;
    const { sessionId } = query;
    if(!sessionId) {
      return h.response('A session id is required to subscribe to campaign events.').code(400);
    }
    const stream = new PassThrough({ objectMode: true });
    stream.write({ type: INIT_EVENT });
    console.log('triggered subscribe view.');
    subscribeToCampaign(campaignHash, sessionId, (message) => {
      stream.write(message);
    });

    return h.event(stream, null, { event: campaignHash });
  },
  endSession: (request, h) => {
    const { params } = request;
    const { sessionId } = params;
    removeSubscriber(sessionId);
    return `Removed subscribers for ${sessionId}.`;
  }
};

export default CampaignEventsView;
