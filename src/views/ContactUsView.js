import { sendSupportMail } from "../utils/mailer";

const ContactUsView = {
  requestSupport: async (request, h) => {
    const { email, body } = request.payload;

    const emailBody = `<p>${email} has requested the following support: ${body}</p>`;
    const confirmationEmail = '<p>Thank you for submitting your question. You should receive an email from support@yourquestbook.com soon.</p>';

    try {
      sendSupportMail(
        'support@yourquestbook.com',
        'Support request',
        emailBody
      );

      sendSupportMail(
        email,
        'You\'re support request has been received.',
        confirmationEmail,
      );

      return 'Support request successfully submitted.';
    } catch (error) {
      console.error(error);
      return h.response('Something went wrong submitting support request.').code(500);
    }
  }
};

export default ContactUsView;