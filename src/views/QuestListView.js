import Quest, { QUEST_STATUS_DRAFT, QUEST_STATUS_ACTIVE } from '../models/Quest';
import { publishCampaignEvent, NEW_QUEST_EVENT } from '../utils/events';

const validateCreateQuest = (
  title,
  url_hash,
) => {
  const errors = [];
  if(!title) {
    errors.push('title is required.');
  }
  if(!url_hash) {
    errors.push('url_hash is required.');
  }
  return errors;
}

const QuestListView = {
  getQuests: async (request, h) => {
    const { params } = request;
    const { campaignHash } = params;
    try {
      const quests = await Quest.getQuestsByCampaign(campaignHash);
      return quests;
    } catch (exception) {
      console.error(exception);
      return h.response(
        'An unknown error occured retrieving quests.'
      ).code(500);
    }
    
  },
  /**
   * POST endpoint for creating a new quest. An event is sent to the client
   * alerting them of the new quest.
   */
  createQuest: async (request, h) => {
    try {
      const { auth, payload, params } = request;
      const { credentials = {}} = auth;
      const { id: userId = '' } = credentials;
      const {
        title = '',
        status = QUEST_STATUS_DRAFT,
      } = payload;
      const { campaignHash } = params;
      const errors = validateCreateQuest(title, campaignHash)
      if(!userId) {
        return h.response('Only a valid user can create quests.').code(403);
      }
      if(errors.length) {
        return h.response(errors).code(400);
      }
      await Quest.insert(
        title,
        status,
        campaignHash,
        userId,
      );
      if(status === QUEST_STATUS_ACTIVE) {
        publishCampaignEvent(campaignHash, {
          type: NEW_QUEST_EVENT,
          title,
        });
      }
      return h.response('Quest created.', 201);
    } catch (error) {
      console.error(error);
      if(error.name === 'ModelNotFoundError') {
        return h.response('Could not find associated campaign.').code(404);
      }
      if(error.name === 'UnauthorizedError') {
        return h.response(
          'User does not have permissions to create a quest for this campaign.'
        ).code(401);
      }
      return h.response(
        'An unknown error occurred while creating the quest.'
      ).code(500);
    }
  }
}

export default QuestListView;
