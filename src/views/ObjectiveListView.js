import Objective, { OBJECTIVE_STATUS_DRAFT, OBJECTIVE_STATUS_ACTIVE } from '../models/Objective';
import Quest from '../models/Quest';
import { publishCampaignEvent, NEW_OBJECTIVE_EVENT } from '../utils/events';

const validateCreateObjective = (label) => {
  if(!label) {
    return ['label is required.'];
  }
  return [];
}

const ObjectiveListView = {
  getObjectives: async (request, h) => {
    const { params } = request;
    const { questId } = params;
    try {
      const objectives = await Objective.listObjectivesByQuest(questId);
      return objectives;
    } catch (error) {
      console.error(error);
      return h.response(
        'An unexpected error occurred while retrieving objectives.'
      ).code(500);
    }
  },
  getAllCampaignObjectives: async (request, h) => {
    const { params } = request;
    const { campaignHash } = params;
    try {
      const objectives = await Objective.getAllCampaignObjectives(campaignHash);
      return objectives
    }
    catch(exception) {
      console.error(exception);
      return h.response(
        'An unexpected error occurred while retrieving objectives.'
      ).code(500);
    }
  },
  createObjective: async (request, h) => {
    const { params, payload, auth } = request;
    const { credentials = {} } = auth;
    const { id: userId = '' } = credentials;
    const { questId = '' } = params;
    const { label = '', status = OBJECTIVE_STATUS_DRAFT } = payload;
    const errors = validateCreateObjective(label);
    if(!userId) {
      return h.response('Only a valid user can create objectives.').code(403);
    }
    if(errors.length) {
      // field errors
      return h.response(errors).code(400);
    }
    try {
      const { owner_id: ownerId, url_hash: campaignHash } = await Quest.getQuestOwner(questId);
      if(ownerId === userId) {
        await Objective.insertObjective(
          label,
          status,
          questId,
        );

        if(status === OBJECTIVE_STATUS_ACTIVE) {
          publishCampaignEvent(campaignHash, {
            type: NEW_OBJECTIVE_EVENT,
            questId,
            label
          });
        };
        return h.response('Objective created.').code(201);
      }
      // owner and user don't match
      return h.response(
        'User does not have permission to add objectives to this quest.'
      ).code(401);
    } catch (error) {
      console.error(error);
      if (error.name === 'ModelNotFoundError') {
        return h.response(
          'The specified quest or campaign could not be found.'
        ).code(404);
      }
      return h.response(
        'An unexpected error occurred while creating the objective.'
      ).code(500);
    }  
  }
}

export default ObjectiveListView;
