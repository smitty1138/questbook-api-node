import Campaign from '../models/Campaign';

const CampaignListView = {
    fetchCampaignsByUser: async (request, h) => {
        try {
            const { credentials = {} } = request.auth;
            const { id } = credentials;
            const campaigns = Campaign.getCampaignsForUser(id);
            return campaigns;
        } catch (error) {
            console.error(error);
            return h.response('An error occurred while retrieving campaigns.').code(500);
        }
        
    },
    createCampaign: async (request, h) => {
        try {
            const { credentials = {}} = request.auth;
            const { id = '', isSubscribed } = credentials;
            const { title = '', description ='' } = request.payload;

            if(!isSubscribed) {
                const campaigns = await Campaign.getCampaignsForUser(id);
                if (campaigns.length) {
                    return h.response('This account is not permitted multiple campaigns.').code(403);
                }
            }

            if (!id) {
                return h.response('You must be a valid user to create a campaign.').code(401);
            }
            if (!title) {
                return h.response('Campaign title is required.').code(400);
            }
            Campaign.insertCampaign(title, description, id);
            return h.response('Campaign created.').code(201);
        } catch (exception) {
            console.error(exception);
            return h.response('An unknown error occurred.').code(500);
        }
    },
};

export default CampaignListView;
