import Campaign from '../models/Campaign';

export const CampaignDetailView = {
  getCampaignByHash: async (request, h) => {
    const { params: { campaignHash } } = request;
    try {
      return Campaign.getCampaignByHash(campaignHash);
    } catch (exception) {
      console.error(exception);
      if(exception.name === 'ModelNotFoundError') {
        return h.response('Could not find campaign.').code(404);
      }
      return h.response('An unknown error occurred.').code(500);
    }
  },
  updateCampaign: async (request, h) => {
    try {
      const { params, auth, payload } = request;
      const { credentials = {} } = auth;
      const { id: userId = '' } = credentials;
      const { campaignHash } = params;
      if(!userId) {
        return h.response('Only a valid user may update campaigns.').code(403);
      }
      const { owner_id: ownerId, id } = await Campaign.getCampaignByHash(campaignHash);
      if(userId === ownerId) {
        await Campaign.update(id, payload);
        return 'Campaign updated successfully.'; 
      } 
      return h.response('User is not permitted to modify this campaign.').code(401);
    } catch (error) {
      console.error(error);
      return h.response(
        'An unknown error occurred while updating campaign.'
      ).code(500);
    }
  },
}

export default CampaignDetailView;