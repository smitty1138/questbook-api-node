import moment from 'moment';
import User from '../models/User';
import PasswordReset from '../models/PasswordReset';
import { sendSupportMail } from '../utils/mailer';

const PASSWORD_RESET_TIME_LIMIT = 15;

export const PasswordResetView = {
  initiatePasswordReset: async (request, h) => {
    const { email } = request.payload;
    try {
      const user = await User.getUserByUsername(email);
      const passwordReset = await PasswordReset.createPasswordReset(user.id);
      const passwordResetUrl = `${process.env.APP_URL}/reset-password/${passwordReset.urlHash}/`;

      //send the reset email
      sendSupportMail(
        email,
        'Password Reset Instructions',
        `<p>Here is your link to reset your Questbook password. If you did not request a password change, we recommend you change it anyway as someone is attempting to access your account.</p><p><a href="${passwordResetUrl}">Click here to reset password.</a></p>`
      );
      return 'Password reset processed';
    } catch (error) {
      console.error(error);
      if(error.name === 'ModelNotFoundError') {
        // don't error out for security reasons.
        return 'Password reset processed.';
      }
      return h.response('An unknown error occurred.').code(500);
    }
  },
  validateUrl: async (request, h) => {
    const { params } = request;
    const { hash } = params;

    try {
      const passwordReset = await PasswordReset.getPasswordResetByHash(hash);
      const now = moment();
      const resetTime = moment(passwordReset.createdAt);
      const duration = now.diff(resetTime, 'm');
      if(duration < PASSWORD_RESET_TIME_LIMIT) {
        return 'Password reset link valid.';
      }
      return h.response('Password reset link has expired.').code(410);
    }
    catch(error) {
      console.error(error);
      if(error.name === 'ModelNotFoundError') {
        return h.response('Could not find associated password reset.').code(404);
      }
      return h.response('An unknown error occurred.').code(500);
    }
  },
  updatePassword: async (request, h) => {
    const { params, payload } = request;
    const { hash } = params;
    const { password } = payload;
    console.log(payload);

    try {
      const passwordReset = await PasswordReset.getPasswordResetByHash(hash);
      const { user_id: userId } = passwordReset;
      const now = moment();
      const resetTime = moment(passwordReset.createdAt);
      const duration = now.diff(resetTime, 'm');
      if(duration < PASSWORD_RESET_TIME_LIMIT) {
        await User.updatePassword(userId, password);
        return 'Password updated successfully.';
      }
      return h.response('Password reset link has expired.').code(410);
    }
    catch(error) {
      console.error(error);
      if(error.name === 'ModelNotFoundError') {
        return h.response('Could not find associated password reset.').code(404);
      }
      return h.response('An unknown error occurred.').code(500);
    }
  }
};

export default PasswordResetView;
