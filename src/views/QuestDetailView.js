import Quest, { QUEST_STATUS_COMPLETED, QUEST_STATUS_DRAFT, QUEST_STATUS_ACTIVE } from '../models/Quest';
import { publishCampaignEvent, COMPLETED_QUEST_EVENT, NEW_QUEST_EVENT } from '../utils/events';

const fireQuestUpdateEvent = (oldQuest, newQuest, campaignHash) => {
  const { status: prev_status, title: oldTitle } = oldQuest;
  const { status, title } = newQuest;
  const eventTitle = title ? title : oldTitle;
  if (prev_status !== QUEST_STATUS_COMPLETED && status === QUEST_STATUS_COMPLETED) {
    publishCampaignEvent(campaignHash, {
      type: COMPLETED_QUEST_EVENT,
      title: eventTitle,
    });
  } else if (prev_status === QUEST_STATUS_DRAFT && status === QUEST_STATUS_ACTIVE) {
    publishCampaignEvent(campaignHash, {
      type: NEW_QUEST_EVENT,
      title: eventTitle,
    });
  }
}

const QuestDetailView = {
  getQuest: async (request, h) => {
    const { params } = request;
    const { id } = params;
    try {
      const quest = Quest.get(id);
      return quest;
    } catch (error) {
      console.error(error);
      return h.response(
        'An unexpected error occurred while retrieving quest.'
      ).code(500);
    }
  },
  updateQuest: async (request, h) => {
    const { params, auth, payload } = request;
    const { credentials = {} } = auth;
    const { id: userId = '' } = credentials;
    const { id } = params;
    if(!userId) {
      return h.response('Only a valid user may update quests.').code(403);
    }
    const { owner_id: ownerId, url_hash: urlHash } = await Quest.getQuestOwner(id);
    const currentQuest = await Quest.get(id);
    if(userId === ownerId) {
      try {
        await Quest.update(id, payload);
        fireQuestUpdateEvent(currentQuest, payload, urlHash);
        return 'Quest updated successfully.';
      } catch (error) {
        console.error(error);
        return h.response(
          'An unknown error occurred while updating quest.'
        ).code(500);
      }
    }
    return h.response('User is not permitted to modify this quest.').code(401);
  }
};

export default QuestDetailView;
