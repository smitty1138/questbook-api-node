import Objective, { OBJECTIVE_STATUS_DRAFT, OBJECTIVE_STATUS_ACTIVE, OBJECTIVE_STATUS_COMPLETED } from '../models/Objective';
import { publishCampaignEvent, COMPLETED_OBJECTIVE_EVENT, NEW_OBJECTIVE_EVENT } from '../utils/events';

const fireObjectiveUpdateEvent = (oldObjective, newObjective, campaignHash) => {
  const { status: oldStatus, label: oldLabel, quest_id: questId } = oldObjective;
  const { status, label } = newObjective;
  const eventLabel = label ? label : oldLabel;
  if (oldStatus === OBJECTIVE_STATUS_ACTIVE && status === OBJECTIVE_STATUS_COMPLETED) {
    publishCampaignEvent(campaignHash, {
      type: COMPLETED_OBJECTIVE_EVENT,
      label: eventLabel,
      questId,
    });
  } else if (
    (oldStatus === OBJECTIVE_STATUS_DRAFT || oldStatus === OBJECTIVE_STATUS_COMPLETED) &&
    status === OBJECTIVE_STATUS_ACTIVE
  ) {
    publishCampaignEvent(campaignHash, {
      type: NEW_OBJECTIVE_EVENT,
      label: eventLabel,
      questId,
    });
  }
}

const ObjectiveDetailView = {
  getObjective: async (request, h) => {
    const { params } = request;
    const { id } = params;
    try {
      const objective = await Objective.get(id);
      return objective;
    } catch (error) {
      console.error(error);
      return h.response(
        'An unexpected error occurred while retrieving objective.'
      ).code(500);
    }
  },
  updateObjective: async (request, h) => {
    const { params, auth, payload } = request;
    const { credentials = {} } = auth;
    const { id: userId = '' } = credentials;
    const { id } = params;
    if(!userId) {
      return h.response('Only a valid user may update objectives.').code(403);
    }
    const { owner_id: ownerId, url_hash: urlHash } = await Objective.getObjectiveOwner(id);
    const currentObjective = await Objective.get(id);
    if(userId === ownerId) {
      try {
        await Objective.update(id, payload);
        fireObjectiveUpdateEvent(currentObjective, payload, urlHash);
        return 'Objective updated successfully.';
      } catch (error) {
        console.error(error);
        return h.response(
          'An unknown error occurred while updating objective.'
        ).code(500);
      }
    }
    return h.response('User is not permitted to modify this objective.').code(401);
  }
};

export default ObjectiveDetailView;
