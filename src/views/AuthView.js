import jwt from 'jsonwebtoken';
import User from "../models/User";
import { jwtSecretKey, validatePassword } from '../utils/auth';
import { sendSupportMail } from '../utils/mailer';

const AuthView = {
    getToken: async (request, h) => {
        const { username, password } = request.payload;
        try {
            const user = await User.getUserByUsername(username);
            const { email, id, is_subscribed } = user;
            if ( await validatePassword(password, user.password)) {
                const token = jwt.sign({
                    id,
                    username,
                }, jwtSecretKey);
                return {
                    token,
                    user: {
                        username,
                        email,
                        id,
                        is_subscribed,
                    }
                };
            }
            return h.response('Invalid credentials').code(400);
        }
        catch(exception) {
            console.error(exception);
            return h.response('Something went wrong logging in user.').code(500);
        } 
    },
    registerUser: async (request, h) => {
        const { username, email, password } = request.payload;
        try {
            await User.createUser(email, username, password);
            const loginUrl = `${process.env.APP_URL}/login/`;

            // send a confirmation email
            sendSupportMail(
                email,
                'Welcome to Questbook!',
                `<p>You\'re all set to start creating campaigns, tracking quests, and connecting your players to your story.</p><p><a href="${loginUrl}">Log in here</a> to begin.</p>`
            );
            return h.response(`User ${username} created.`).code(201);
        } catch (error) {
            console.error(error);
            if(error.name === 'ExistingUserError') {
                return h.response('User already exists.').code(409);
            }
            return h.response('Something went wrong creating user.').code(500);
        }
    },
    validateToken: async (request, h) => {
        const { auth } = request;
        const { credentials } = auth;
        const { id } = credentials;
        try {
            const user = await User.getUser(id);
            return user;
        }
        catch (exception) {
            console.error(exception);
            return h.response('Something went wrong authorizing this token.').code(500);
        }
    },
};

export default AuthView;
