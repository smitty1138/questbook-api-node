import bcrypt from 'bcrypt';
import User from '../models/User';

const SALT_ROUNDS = 10;

export const validateToken = async (decoded) => {
    const { id } = decoded;
    const user = await User.getUser(id);
    const { username, is_subscribed: isSubscribed } = user;
    if(user) {
        return {
            isValid: true,
            credentials: {
                id,
                username,
                isSubscribed
            }
        };
    }
    return {
        isValid: false,
    };
};

export const jwtSecretKey = process.env.JWT_AUTH_SECRET;

export const hashPassword = async (password) => {
    const hash = await bcrypt.hash(password, SALT_ROUNDS);
    return hash;
}

export const validatePassword = async (password, hash) => {
    const result = await bcrypt.compare(password, hash);
    return result;
}

export default {
    validateToken,
    jwtSecretKey,
};
