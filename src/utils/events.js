import { EventEmitter } from 'events';
const eventsEmitter = new EventEmitter();

//event types
export const NEW_OBJECTIVE_EVENT = 'NEW_OBJECTIVE';
export const COMPLETED_OBJECTIVE_EVENT = 'COMPLETED_OBJECTIVE';
export const NEW_QUEST_EVENT = 'NEW_QUEST';
export const COMPLETED_QUEST_EVENT = 'COMPLETED_QUEST';
export const INIT_EVENT = 'INIT';

// track the session of anyone who has subscribed
// to a campaign.
export const campaignSubscribers = {};

export const publishCampaignEvent = (campaignHash, message) => {
  eventsEmitter.emit(campaignHash, message);
};

export const removeSubscriber = (session) => {
  if(session in campaignSubscribers) {
    const { event, listener, failsafe } = campaignSubscribers[session];
    eventsEmitter.removeListener(event, listener);

    // this prevents the failsafe from interrupting an actively subscribed user.
    if(failsafe) {
      clearTimeout(failsafe);
    }
    console.log('removed listener from events');
    console.log(`listener count: ${eventsEmitter.listenerCount(event)}`);
  }
}

export const subscribeToCampaign = (campaignHash, session, handler) => {
  removeSubscriber(session);
  eventsEmitter.on(campaignHash, handler);

  // follow up calls happen every 33 seconds, we will wait 1 minute
  // and if we don't get another session request, we kill the zombies
  const failsafe = setTimeout(() => {
    removeSubscriber(session);
  }, 300000);

  campaignSubscribers[session] = {
    event: campaignHash,
    listener: handler,
    failsafe,
  }
  console.log('added listener to campaign events');
  console.log(`listener count: ${eventsEmitter.listenerCount(campaignHash)}`);
};

export default {
  publishCampaignEvent,
  subscribeToCampaign,
  campaignSubscribers,
  removeSubscriber,
};
