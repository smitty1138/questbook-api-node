import mailgun from 'mailgun-js';

const DOMAIN = 'mg.yourquestbook.com';
const mg = mailgun({apiKey: process.env.MAILGUN_API_KEY, domain: DOMAIN});

export const sendSupportMail = async (to, subject, html) => {
  mg.messages().send({
    from: '"Questbook Support"<support@yourquestbook.com>',
    to,
    subject,
    html,
  }, (error, body) => {
    console.error(error);
    console.log(body);
  });
};

export const sendInvite = async (to, campaign, gm) => {
  const { first_name: firstName, email } = gm;
  const { url_hash: urlHash, title } = campaign;
  const inviteUrl = `${process.env.APP_URL}/${urlHash}/quests/`;

  mg.messages().send({
    from: '"Questbook"<no-reply@yourquestbook.com>',
    to,
    subject: 'You\'ve been invited to a campaign on Questbook!',
    template: 'invite_player',
    'v:gmFirstName': firstName || email,
    'v:inviteUrl': inviteUrl,
    'v:campaignName': title,
  }, (error, body) => {
    console.error(error);
    console.log(body);
  });
};
