import moment from 'moment';
import Knex from '../knex';
import { hashPassword } from '../utils/auth';
import ExistingUserError from '../exceptions/ExistingUserError';
import ModelNotFoundError from '../exceptions/ModelNotFoundError';

const USER_TABLE = 'auth_user';

const User = {
    getUserByUsername: async (username) => {
        const users = await Knex(USER_TABLE).where({
            username,
        }).select('username', 'password', 'id', 'email', 'is_subscribed');
        if(users.length === 1) {
            return users[0];
        } else {
            throw new ModelNotFoundError('Couldn\'t find user.');
        }
    },
    createUser: async (email, username, password, firstName = '', lastName = '') => {
        const passwordHash = await hashPassword(password);
        const sqlDate = moment().format('YYYY/MM/DD HH:mm:ss');
        const existingUser = await Knex(USER_TABLE).where({
            email,
        }).select('id');
        if(existingUser.length) {
            throw new ExistingUserError();
        }
        return await Knex(USER_TABLE).insert({
            username,
            password: passwordHash,
            email,
            first_name: firstName,
            last_name: lastName,
            date_joined: sqlDate,
        });
    },
    getUser: async (userId) => {
        const users = await Knex(USER_TABLE).where({
            id: userId,
        }).select('id', 'username', 'email', 'is_subscribed', 'first_name', 'last_name');
        if(users.length === 1) {
            return users[0];
        } else {
            throw ModelNotFoundError('Couldn\'t find user.');
        }
    },
    updatePassword: async (userid, password) => {
        const passwordHash = await hashPassword(password);
        await Knex(USER_TABLE).where({
            id: userid,
        }).update({
            password: passwordHash,
        });
    },
};

export default User;
