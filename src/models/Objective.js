import Knex from '../knex';
import { QUEST_TABLE } from './Quest';
import { CAMPAIGN_TABLE } from './Campaign';

const OBJECTIVE_TABLE = 'api_objectivemodel';

export const OBJECTIVE_STATUS_DRAFT = 'draft';
export const OBJECTIVE_STATUS_ACTIVE = 'active';
export const OBJECTIVE_STATUS_COMPLETED = 'completed';

const Objective = {
  listObjectivesByQuest: async (quest_id) => {
    const objectives = await Knex(OBJECTIVE_TABLE).where({
      quest_id,
    }).select(
      'id',
      'label',
      'status',
      'quest_id',
    );
    return objectives;
  },
  insertObjective: async (
    label,
    status,
    quest_id,
  ) => {
    await Knex(OBJECTIVE_TABLE).insert({
      label,
      status,
      quest_id,
    });
  },
  get: async id => {
    const results = await Knex(OBJECTIVE_TABLE).where({ id }).select(
      'id',
      'label',
      'status',
      'quest_id',
    )
    if(results.length === 1) {
      return results[0];
    }
    throw Error(`Received unexpected number of rows for objective ${id}`);
  },
  update: async (id, updateData) => {
    await Knex(OBJECTIVE_TABLE).where({ id }).update(updateData);
  },
  getObjectiveOwner: async id => {
    const results = await Knex(OBJECTIVE_TABLE).join(
      QUEST_TABLE,
      `${QUEST_TABLE}.id`,
      '=',
      `${OBJECTIVE_TABLE}.quest_id`,
    ).join(
      CAMPAIGN_TABLE,
      `${CAMPAIGN_TABLE}.id`,
      '=',
      `${QUEST_TABLE}.campaign_id`,
    ).where(
      `${OBJECTIVE_TABLE}.id`,
      id,
    ).select(`${CAMPAIGN_TABLE}.*`);
    if(results.length === 1) {
      return results[0];
    }
    if(results.length === 0) {
      throw new ModelNotFoundError(`Owner not found for objective ${id}`);
    }
    throw new Error(`Received unexpected number of owners for objective ${id}`);
  },
  getAllCampaignObjectives: async campaignHash => {
    const results = await Knex(OBJECTIVE_TABLE).join(
      QUEST_TABLE,
      `${QUEST_TABLE}.id`,
      '=',
      `${OBJECTIVE_TABLE}.quest_id`,
    ).join(
      CAMPAIGN_TABLE,
      `${CAMPAIGN_TABLE}.id`,
      '=',
      `${QUEST_TABLE}.campaign_id`,
    ).where(
      `${CAMPAIGN_TABLE}.url_hash`,
      campaignHash
    ).select(
      `${OBJECTIVE_TABLE}.id`,
      `${OBJECTIVE_TABLE}.label`,
      `${OBJECTIVE_TABLE}.status`,
      `${OBJECTIVE_TABLE}.quest_id`,
    );
    return results;
  }
}

export default Objective;
