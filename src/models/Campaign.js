import crypto from 'crypto';
import moment from 'moment';
import Knex from '../knex';
import ModelNotFoundError from '../exceptions/ModelNotFoundError';

export const CAMPAIGN_TABLE = 'api_campaignmodel';

const createCampaignHash = (title, userid) => {
    const now = moment().format('YYYY/MM/DD HH:mm:ss');
    const toHash = `${title}${userid}${now}`;
    const hash = crypto.createHash('md5').update(toHash).digest('hex');
    return hash;
}

const Campaign = {
    getCampaignsForUser: async (userid) => {
        try {
            const results = await Knex(CAMPAIGN_TABLE).where({
                'owner_id': userid,
            }).select(
                'id',
                'title',
                'description',
                'url_hash',
                'owner_id',
            );
    
            return results;
        }
        catch (error) {
            console.error(error);
            throw(error);
        }
    },
    insertCampaign: async (title, description, userid) => {
        await Knex(CAMPAIGN_TABLE).insert({
            title,
            description,
            owner_id: userid,
            url_hash: createCampaignHash(title, userid),
        });
    },
    getCampaignByHash: async (url_hash) => {
        const campaigns = await Knex(CAMPAIGN_TABLE).where({
            url_hash
        }).select(
            'title',
            'description',
            'id',
            'owner_id',
            'url_hash',
            'is_archived',
        );
        if(campaigns.length === 1) {
            return campaigns[0];
        }
        if(campaigns.length === 0) {
            throw new ModelNotFoundError(
                `Campaign ${url_hash} not found.`
            );
        }
        if(campaigns.length > 1) {
            throw new Error(
                `Unexpected number of campaigns found for hash. Expected 1, found ${campaigns.length}`
            );
        }
    },
    update: async (id, updateData) => {
        await Knex(CAMPAIGN_TABLE).where({ id }).update(updateData);
    },
    get: async id => {
    const results = await Knex(CAMPAIGN_TABLE).where({ id }).select(
        'id',
        'title',
        'is_archived',
        'owner_id',
        'description',
        'url_hash',
    );
    if(results.length === 1) {
        return results[0];
    }
    throw Error(`Received unexpected number of rows for quest ${id}`);
    },
};

export default Campaign;
