import Knex from '../knex';
import Campaign, { CAMPAIGN_TABLE } from './Campaign';
import UnauthorizedError from '../exceptions/UnauthorizedError';
import ModelNotFoundError from '../exceptions/ModelNotFoundError';

export const QUEST_TABLE = 'api_questmodel';
export const QUEST_STATUS_DRAFT = 'draft';
export const QUEST_STATUS_ACTIVE = 'active';
export const QUEST_STATUS_COMPLETED = 'completed';

const Quest = {
  getQuestsByCampaign: async (campaignHash) => {
    try {
      const quests = await Knex(QUEST_TABLE).join(
        CAMPAIGN_TABLE,
        `${QUEST_TABLE}.campaign_id`,
        '=',
        `${CAMPAIGN_TABLE}.id`,
      ).where(`${CAMPAIGN_TABLE}.url_hash`, campaignHash)
      .select(`${QUEST_TABLE}.id`, `${QUEST_TABLE}.title`, 'campaign_id', `${QUEST_TABLE}.status`);
      return quests;
    }
    catch (exception) {
      console.error(exception);
    }
  },
  insert: async (
    title,
    status,
    url_hash,
    userId,
  ) => {
    const campaign = await Campaign.getCampaignByHash(url_hash);
    const { owner_id, id } = campaign;
    if(owner_id === userId) {
      await Knex(QUEST_TABLE).insert({
        title,
        status,
        campaign_id: id,
      });
    } else {
      throw new UnauthorizedError(
        `User ${userId} is not permitted to add quests to ${url_hash}`
      );
    }
  },
  get: async id => {
    const results = await Knex(QUEST_TABLE).where({ id }).select(
      'id',
      'title',
      'campaign_id',
      'status',
    );
    if(results.length === 1) {
      return results[0];
    }
    throw Error(`Received unexpected number of rows for quest ${id}`);
  },
  update: async (id, updateData) => {
    await Knex(QUEST_TABLE).where({ id }).update(updateData);
  },
  getQuestOwner: async (id) => {
    const results = await Knex(QUEST_TABLE).join(
      CAMPAIGN_TABLE,
      `${QUEST_TABLE}.campaign_id`,
      '=',
      `${CAMPAIGN_TABLE}.id`,
    ).where(
      `${QUEST_TABLE}.id`,
      id,
    ).select(`${CAMPAIGN_TABLE}.*`);
    if(results.length === 1) {
      return results[0];
    }
    if(results.length === 0) {
      throw new ModelNotFoundError(`Owner not found for quest ${id}`);
    }
    throw new Error(`Received unexpected number of owners for quest ${id}`);
  }
};

export default Quest;
