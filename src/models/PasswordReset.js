import moment from 'moment';
import uuid from 'uuid/v4';
import Knex from '../knex';
import ModelNotFoundError from '../exceptions/ModelNotFoundError';

const PASSWORD_RESET_TABLE = 'password_resets';

export const PasswordReset = {
  createPasswordReset: async (userId) => {
    const urlHash = uuid();
    const sqlDate = moment().format('YYYY/MM/DD HH:mm:ss');
    await Knex(PASSWORD_RESET_TABLE).insert({
        user_id: userId,
        createdAt: sqlDate,
        url: urlHash,
    });

    return {
      urlHash,
    };
  },
  getPasswordResetByHash: async (urlHash) => {
    const results = await Knex(PASSWORD_RESET_TABLE).where({
      url: urlHash,
    }).select('createdAt', 'user_id');
    if(results.length == 1) {
      return results[0];
    }
    throw new ModelNotFoundError('Couldn\'t find password reset record.');
  },
}

export default PasswordReset;