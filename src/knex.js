export default require( 'knex' )({
    client: 'mysql',
    connection: {
        host: process.env.QUESTBOOK_DB_HOST,
        user: process.env.QUESTBOOK_DB_USER,
        password: process.env.QUESTBOOK_DB_PASS,
        port: process.env.QUESTBOOK_DB_PORT || 3306,
        database: 'questbook',
        charset: 'utf8',
    },
    migrations: {
        tableName: 'knex_migrations',
    }
});
