class ExistingUserError extends Error {
  constructor(...params) {
    super(...params);

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, ExistingUserError);
    }

    this.name = 'ExistingUserError';
  }
}

export default ExistingUserError;
