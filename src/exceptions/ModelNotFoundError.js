class ModelNotFoundError extends Error {
  constructor(...params) {
    super(...params);

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, ModelNotFoundError);
    }

    this.name = 'ModelNotFoundError';
  }
}

export default ModelNotFoundError;
