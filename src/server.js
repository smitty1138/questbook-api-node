import Hapi from 'hapi';
import { validateToken, jwtSecretKey } from './utils/auth';
import routes from './routes';

const init = async () => {
    try {
        const server = new Hapi.server({
            port: 8000,
            routes: {
                cors: true
            },
            debug: {
                log: ['hapi', 'error', 'debug', 'info', 'warning', 'request', 'server', 'timeout', 'internal', 'implementation', 'tail', 'remove', 'last', 'add'],
                request: ['hapi', 'error', 'debug', 'info', 'warning', 'request', 'server', 'timeout', 'internal', 'implementation', 'tail', 'remove', 'last', 'add', 'received', 'handler', 'response']
            },
        });
    
        await server.register(require( 'hapi-auth-jwt2' ));
    
        server.auth.strategy('jwt', 'jwt',
            {
                key: jwtSecretKey,
                validate: validateToken,
                verifyOptions: { algorithms: [ 'HS256' ] }
            }
        );
    
        server.auth.default('jwt');
    
        server.route(routes);
        await server.register({
            plugin: require('hapi-cors'),
            options: {
                methods: ['POST, GET, OPTIONS', 'PUT', 'DELETE'],
            }
        });

        await server.register(require('susie'));
    
        await server.start();
        console.log( `Server started at ${ server.info.uri }` );
    } catch(err) {
        console.log(err);
        process.exit(1);
    }
};

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});

init();
