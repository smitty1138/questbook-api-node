import objectiveRoutes from './objectiveRoutes';
import questRoutes from './questRoutes';
import campaignRoutes from './campaignRoutes';
import authRoutes from './authRoutes';
import supportRoutes from './supportRoutes';

export default [
  {
    method: 'GET',
    path: '/',
    config: {
        auth: false,
    },
    handler: () => {
        return 'Questbook API is running.';
    },
  },
  {
    method: 'GET',
    path: '/health',
    config: {
        auth: false,
    },
    handler: () => {
        return 'Questbook API is running.';
    },
  },
  ...questRoutes,
  ...objectiveRoutes,
  ...campaignRoutes,
  ...authRoutes,
  ...supportRoutes,
];
