import ContactUsView from '../views/ContactUsView';

export default [
  {
    method: 'POST',
    path: '/support/requests/',
    config: {
      auth: false,
    },
    handler: ContactUsView.requestSupport,
  },
];
