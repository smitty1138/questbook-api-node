import ObjectiveListView from '../views/ObjectiveListView';
import ObjectiveDetailView from '../views/ObjectiveDetailView';

export default [
  {
    method: 'GET',
    path: '/quests/{questId}/objectives/',
    config: {
      auth: false,
    },
    handler: ObjectiveListView.getObjectives,
  },
  {
    method: 'POST',
    path: '/quests/{questId}/objectives/',
    config: {
      auth: 'jwt',
    },
    handler: ObjectiveListView.createObjective,
  },
  {
    method: 'GET',
    path: '/campaigns/{campaignHash}/objectives/',
    config: {
      auth: false,
    },
    handler: ObjectiveListView.getAllCampaignObjectives,
  },
  {
    method: 'GET',
    path: '/objectives/{id}/',
    config: {
      auth: false,
    },
    handler: ObjectiveDetailView.getObjective,
  },
  {
    method: 'PUT',
    path: '/objectives/{id}/',
    config: {
      auth: 'jwt',
    },
    handler: ObjectiveDetailView.updateObjective,
  },
];