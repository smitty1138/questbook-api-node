import QuestDetailView from '../views/QuestDetailView';
import QuestListView from '../views/QuestListView';

export default [
  {
    method: 'GET',
    path: '/campaigns/{campaignHash}/quests/',
    config: {
      auth: false,
    },
    handler: QuestListView.getQuests,
  },
  {
    method: 'POST',
    path: '/campaigns/{campaignHash}/quests/',
    config: {
      auth: 'jwt',
    },
    handler: QuestListView.createQuest,
  },
  {
    method: 'GET',
    path: '/quests/{id}/',
    config: {
      auth: false,
    },
    handler: QuestDetailView.getQuest,
  },
  {
    method: 'PUT',
    path: '/quests/{id}/',
    config: {
      auth: 'jwt',
    },
    handler: QuestDetailView.updateQuest,
  },
];
