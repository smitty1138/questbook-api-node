import CampaignListView from '../views/CampaignListView';
import CampaignDetailView from '../views/CampaignDetailView';
import CampaignEventsView from '../views/CampaignEventsView';
import CampaignShareView from '../views/CampaignShareView';

export default [
  {
    method: 'GET',
    path: '/campaigns/',
    config: {
      auth: 'jwt',
    },
    handler: CampaignListView.fetchCampaignsByUser,
  },
  {
    method: 'POST',
    path: '/campaigns/',
    config: {
      auth: 'jwt',
    },
    handler: CampaignListView.createCampaign,
  },
  {
    method: 'GET',
    path: '/campaigns/{campaignHash}/',
    config: {
      auth: false,
    },
    handler: CampaignDetailView.getCampaignByHash,
  },
  {
    method: 'PUT',
    path: '/campaigns/{campaignHash}/',
    config: {
      auth: 'jwt',
    },
    handler: CampaignDetailView.updateCampaign,
  },
  {
    method: 'GET',
    path: '/campaigns/{campaignHash}/events/',
    config: {
      auth: false,
    },
    handler: CampaignEventsView.subscribeToCampaign,
  },
  {
    method: 'POST',
    path: '/campaigns/{campaignHash}/share/',
    config: {
      auth: 'jwt',
    },
    handler: CampaignShareView.shareCampaign,
  },
  {
    method: 'DELETE',
    path: '/session/{sessionId}/events/',
    config: {
      auth: false,
    },
    handler: CampaignEventsView.endSession,
  }
];
