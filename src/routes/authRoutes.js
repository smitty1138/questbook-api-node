import AuthView from '../views/AuthView';
import PasswordResetView from '../views/PasswordResetView';

export default [
  {
    method: 'POST',
    path: '/token-auth/',
    config: {
        auth: false,
    },
    handler: AuthView.getToken,
  },
  {
    method: 'POST',
    path: '/users/register/',
    config: {
      auth: false,
    },
    handler: AuthView.registerUser,
  },
  {
    method: 'POST',
    path: '/token-verify/',
    config: {
      auth: 'jwt',
    },
    handler: AuthView.validateToken,
  },
  {
    method: 'POST',
    path: '/password-reset/',
    config: {
      auth: false,
    },
    handler: PasswordResetView.initiatePasswordReset,
  },
  {
    method: 'GET',
    path: '/password-reset/{hash}/',
    config: {
      auth: false,
    },
    handler: PasswordResetView.validateUrl,
  },
  {
    method: 'POST',
    path: '/password-reset/{hash}/',
    config: {
      auth: false,
    },
    handler: PasswordResetView.updatePassword,
  },
];
