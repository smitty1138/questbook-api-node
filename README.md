# questbook-api-node
An api for creating and sharing rpg campaigns, quests and objectives.

## environment setup
Questbook requires a few environment variables to be set so that it can access its database and provide authentication.
```
export QUESTBOOK_DB_USER=<database_username>
export QUESTBOOK_DB_PASS=<database_password>
export JWT_AUTH_SECRET=<secret_key>
export QUESTBOOK_DB_HOST=<database_host>
export QUESTBOOK_DB_PORT=<database_port>
export MAILGUN_API_KEY=<mailgun_api_key>
export APP_URL=<url_to_the_client_app>
```

You also need to create **knexfile.js** in the root of the project with configuration pointing to your development database.

If you are running a local database, you will need to prep the database by running the **migrations/legacy/initial_schema.sql** file againse your local
database.

## to run
```
npm install #install node dependencies
npm run start # start server on port 8000
```

## running with docker
```
# build and tag the image
docker build -t questbook-api-node:latest .

# run providing the necessary env variables
docker run -p 8000:8000 -env QUESTBOOK_DB_USER=$QUESTBOOK_DB_USER -env QUESTBOOK_DB_PASS=$QUESTBOOK_DB_PASS -env JWT_AUTH_SECRET=$JWT_AUTH_SECRET questbook-api-node:latest
```
## running database migrations
```
knex migrate:latest
```

## dealing with mysql client authentication errors
If knex runs into an authentication error and blames the mysql client version, try running this query against your database
```
ALTER USER 'user'@'%' IDENTIFIED WITH mysql_native_password BY 'password';
flush privileges;
```

## upload production docker image
```
# build and tag the image
docker build -t questbook-api-node:production .
docker tag questbook-api-node:production gcr.io/questbook-255200/questbook-api:production

#push to google cloud
docker push gcr.io/questbook-255200/questbook-api:production
```
