eval $(aws ecr get-login --no-include-email --region us-east-2)
docker build -t questbook-api-node:master .
docker tag questbook-api-node:master $QUESTBOOK_DOCKER_REPO:master
docker push $QUESTBOOK_DOCKER_REPO:master