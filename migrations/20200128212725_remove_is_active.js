
exports.up = function(knex) {
  return knex.schema.table('auth_user', function(t) {
    t.dropColumn('is_active');
  });
};

exports.down = function(knex) {
  return knex.schema.table('auth_user', function(t) {
    t.boolean('is_active').notNull().defaultTo(false);
  });
};
