exports.up = function(knex) {
  return knex.schema.table('api_questmodel', function(t) {
    t.string('status').notNull().defaultTo('draft');
    t.dropColumn('is_active');
    t.dropColumn('is_archived');
  });
};

exports.down = function(knex) {
  return knex.schema.table('api_questmodel', function(t) {
    t.dropColumn('status');
    t.boolean('is_active').defaultTo(false);
    t.boolean('is_archived').defaultTo(false);
  });
};
