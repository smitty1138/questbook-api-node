
exports.up = function(knex) {
  return knex.schema.createTable('password_resets', t => {
    t.increments('id').unsigned().primary();
    t.dateTime('createdAt').notNull();
    t.integer('user_id').notNull();
    t.string('url').notNull();
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable('password_resets');
};
