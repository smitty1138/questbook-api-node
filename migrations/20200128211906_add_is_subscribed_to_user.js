
exports.up = function(knex) {
  return knex.schema.table('auth_user', function(t) {
    t.boolean('is_subscribed').notNull().defaultTo(false);
  });
};

exports.down = function(knex) {
  return knex.schema.table('auth_user', function(t) {
    t.dropColumn('is_subscribed');
  });
};
