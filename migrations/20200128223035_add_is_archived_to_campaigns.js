
exports.up = function(knex) {
  return knex.schema.table('api_campaignmodel', function(t) {
    t.boolean('is_archived').notNull().defaultTo(false);
  });
};

exports.down = function(knex) {
  return knex.schema.table('api_campaignmodel', function(t) {
    t.dropColumn('is_archived');
  });
};
