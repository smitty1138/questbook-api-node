
exports.up = function(knex) {
  return knex.schema.table('auth_user', function(t) {
    t.dropColumn('is_superuser');
  });
};

exports.down = function(knex) {
  return knex.schema.table('auth_user', function(t) {
    t.boolean('is_superuser').notNull().defaultTo(false);
  });
};
