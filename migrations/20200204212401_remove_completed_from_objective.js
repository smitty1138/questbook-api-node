exports.up = function(knex) {
  return knex.schema.table('api_objectivemodel', function(t) {
    t.string('status').notNull().defaultTo('draft');
    t.dropColumn('is_completed');
    t.dropColumn('is_active');
  });
};

exports.down = function(knex) {
  return knex.schema.table('api_objectivemodel', function(t) {
    t.dropColumn('status');
    t.boolean('is_completed').defaultTo(false);
    t.boolean('is_active').defaultTo(false);
  });
};
