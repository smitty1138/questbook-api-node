
exports.up = function(knex) {
  return knex.schema.table('auth_user', function(t) {
    t.dropColumn('is_staff');
  });
};

exports.down = function(knex) {
  return knex.schema.table('auth_user', function(t) {
    t.boolean('is_staff').notNull().defaultTo(false);
  });
};
