FROM node:10-alpine

WORKDIR /usr/src/app

COPY package*.json ./
RUN apk --no-cache --virtual build-dependencies add \
    python \
    make \
    g++ \
    && npm install \
    && apk del build-dependencies

COPY . .

CMD [ "npm", "start" ]
